// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.DoubleSolenoid.Value;

/*
 * The Constants class provides a convenient place for teams to hold robot-wide
 * numerical or boolean constants. This class should not be used for any other purpose. 
 * All constants should be declared globally (i.e. public static). Do not put anything 
 * functional in this class. It is advised to statically import this class (or one of 
 * its inner classes) wherever the constants are needed, to reduce verbosity.
 */
public final class Constants {

    // DriveUtil

    public static int leftPrimary = 2;
    public static int leftSecondary = 3;
    public static int rightPrimary = 4;
    public static int rightSecondary = 5;

    public static double speedLimit = 1.0;

    public static int driver = 0;
    public static int operator = 1;
    public static int leftJoystick = 2;
    public static int rightJoystick = 3;

    public static int leftEncoder_channelA = 0;
    public static int leftEncoder_channelB = 1;
    public static int rightEncoder_channelA = 2;
    public static int rightEncoder_channelB = 3;

    public static int tickfeet = 215;
    
    public static boolean shouldCoerce = false;

    // MagUtil

    public static int brushMotor = 10;
    public static int outerIndex = 9;
    public static int innerIndex = 8;

    public static int outerBanner = 4;
    public static int innerBanner = 5;

    public static double indexMotorSpeed = 0.9;
    public static double brushMotorSpeed = 0.75;

    // ShotUtil
    // MOTORS
    public static int primaryShot = 6;
    public static int secondaryShot = 7;
    public static double staticShotSpeed = 0.25;

    // SHOT CONSTANTS
    // Close Low:
    public static int closeLowRPM = 1475;
    // Close High:
    public static int closeHighRPM = 3800;
    // Tarmac High:
    public static int tarmacHighRPM = 2514;

    // Autonomous RPM:
    public static int autoHighRPM = 2700;

    // Safety High:
    public static int safetyHighRPM = 4800;
    // Dynamic RPM:
    public static int dynamicMinRPM = 2500;
    public static int dynamicMaxRPM = 3500;

    // ClimbUtil
    public static int leftClimbForwardChannel = 1;
    public static int leftClimbReverseChannel = 0;

    public static int rightClimbForwardChannel = 3;
    public static int rightClimbReverseChannel = 2;

    public static Value pistonExtended = Value.kForward;
    public static Value pistonRetracted = Value.kReverse;

    // LEDUtil
    public static int ledStripPWM = 9;
    public static int amountOfSegments = 7;
    public static int sideSegmentLength = 10;
    public static int underSegmentLength = 26;
    // Segment positions
    public static int leftTopPos = 1;
    public static int leftBottomPos = 0;
    public static int rightTopPos = 3;
    public static int rightBottomPos = 2;
    public static int underPos = 4;

    // VisionUtil
    // camMode
    public static int driverCam = 1;
    public static int visionCam = 0;
    public static int driverLED = 1;
    public static int visionLED = 3;

    // Auto Drive Values
    public static double autoDriveSpeed = 0.3;
    public static double autoFastDriveSpeed = 0.5;
    public static double autoPivotSpeed = 0.23;
    public static double distanceDeadBandRange = 2;
    public static double angleDeadBandRange = 1.5;
    public static double targetArea = 6;

    public static double autoSlowTurnSpeed = 0.1;
    public static double autoFastTurnSpeed = 0.35;

    public static double autoC2RMotorOutputMin = -0.25;
    public static double autoC2RMotorOutputMax = 0.25;
    // TODO: Fix below C2RGyro values... currently they are set to values from Power Up but I have no idea what they actually should be...
    public static double autoC2RGyroInputMin = -15.0;
    public static double autoC2RGyroInputMax = 15.0;

    // Auto Command Values
    public static double exitDistance = 5;
    public static double autoLowLineUp = 5.0;
    public static double autoTurnRight = 170;
    public static double autoVisionTurnRight = 140;

    // degrees
    public static double autoTurnLeft = -120; // degrees

    // Ten Point Auto Values
    public static double autoTenLeftGyroValue = -192;
    public static double autoTenMidGyroValue = -197;
    public static double autoTenRightGyroValue = -163;

}
