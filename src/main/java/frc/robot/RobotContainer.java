// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import com.ctre.phoenix.motorcontrol.NeutralMode;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.XboxController.Button;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInWidgets;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.commands.NoTiltProtect;
import frc.robot.commands.auto.groups.AutoExit;
import frc.robot.commands.auto.groups.AutoGyroTenLeft;
import frc.robot.commands.auto.groups.AutoGyroTenMid;
import frc.robot.commands.auto.groups.AutoGyroTenRight;
import frc.robot.commands.auto.groups.AutoLowLeft;
import frc.robot.commands.mag.OperateMagazine;
import frc.robot.commands.vision.CenterToVision;
import frc.robot.handlers.ClimbState;
import frc.robot.handlers.ShotMode;
import frc.robot.handlers.SkittleColor;
import frc.robot.handlers.drive.DriveMode;
import frc.robot.handlers.leds.LEDMode;
import frc.robot.subsystems.ClimbUtil;
import frc.robot.subsystems.DriveUtil;
import frc.robot.subsystems.LEDUtil;
import frc.robot.subsystems.MagUtil;
import frc.robot.subsystems.PIDShotUtil;
import frc.robot.subsystems.VisionUtil;

/**
 * This class is where the bulk of the robot should be declared. Since
 * Command-based is a "declarative" paradigm, very little robot logic should
 * actually be handled in the {@link Robot} periodic methods (other than the
 * scheduler calls). Instead, the structure of the robot (including subsystems,
 * commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  // The robot's subsystems and commands are defined here...
  private final DriveUtil driveUtil = new DriveUtil();
  private final ClimbUtil climbUtil = new ClimbUtil();
  private final PIDShotUtil pidShotUtil = new PIDShotUtil();
  private final MagUtil magUtil = new MagUtil();
  private final LEDUtil ledUtil = new LEDUtil();
  private final VisionUtil visionUtil = new VisionUtil();

  private static XboxController driver;
  private static Joystick rightJoystick;
  private static Joystick leftJoystick;
  private static Joystick operator;

  // Button Bindings
  private JoystickButton toggleIntakeIntent, toggleShotIntent, // State override toggles (What we want the robot to do)
      climbUp, climbDown, // Climb commands
      closeLow, closeHigh, tarmacHigh, safetyHigh, shotOff, dynamicShot, // Shot modes
      centerToVision, takeSnapshot, // Vision commands
      noTiltProtect; 

  // State Machine Values
  // does the operator want brush motor to run
  private static boolean isCollecting = false;
  // does the operator want ball to be shot
  private static boolean isShooting = false;
  // proper RPM for shot
  private static boolean isShotReady = false;
  // is the outer banner sensor tripped
  private static boolean hasOuterBall = false;
  // is the inner banner sensor tripped
  private static boolean hasInnerBall = false;
  // what color does the inner sensor see
  private static SkittleColor innerColor = SkittleColor.OTHER;
  // what color does the outer sensor see
  private static SkittleColor outerColor = SkittleColor.OTHER;
  // how many ticks we drove to get cargo
  private static double autoCargoTicks = 0;
  // do we want to tilt protect
  private static boolean tiltProtect = true;

  // Autonomous Config
  private SendableChooser<Command> autoChooser;

  // Drive Mode Config
  private static SendableChooser<DriveMode> driveMode;

  // Shuffleboard Configuration
  ShuffleboardTab driverTab, sensorTab;

  /**
   * The container for the robot. Contains subsystems, OI devices, and commands.
   */
  public RobotContainer() {
    // Controller Setup
    driver = new XboxController(Constants.driver);
    operator = new Joystick(Constants.operator);
    leftJoystick = new Joystick(Constants.leftJoystick);
    rightJoystick = new Joystick(Constants.rightJoystick);

    // Configure the button bindings
    configureButtonBindings();
    setSubsystemDefaultCommands();

    setupAutoSendable();
    setupDriveSendable();
    setupShuffleboard();
  }

  /**
   * Use this method to define your button->command mappings. Buttons can be
   * created by instantiating a {@link GenericHID} or one of its subclasses
   * ({@link edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then
   * passing it to a {@link edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
    // DRIVER & OPERATOR
    // Climb
    climbUp = new JoystickButton(operator, Button.kStart.value);
    climbUp.whenPressed(new InstantCommand(() -> climbUtil.runClimb(ClimbState.DEPLOYED), climbUtil));
    climbDown = new JoystickButton(operator, Button.kBack.value);
    climbDown.whenPressed(new InstantCommand(() -> climbUtil.runClimb(ClimbState.RETRACTED), climbUtil));

    // DRIVER
    centerToVision = new JoystickButton(rightJoystick, 5);
    centerToVision.whenPressed(new CenterToVision(visionUtil, driveUtil));
    // takeSnapshot = new JoystickButton(driver, Button.kB.value);
    // takeSnapshot.whenPressed(new InstantCommand(() -> visionUtil.takeSnapshot(), visionUtil));
    noTiltProtect = new JoystickButton(rightJoystick, 2);
    noTiltProtect.whileHeld(new NoTiltProtect());

    // OPERATOR
    // Shot Mode
    shotOff = new JoystickButton(operator, 1);
    shotOff.whenPressed(new InstantCommand(() -> pidShotUtil.setShotMode(ShotMode.OFF), pidShotUtil));
    closeLow = new JoystickButton(operator, 2);
    closeLow.whenPressed(new InstantCommand(() -> pidShotUtil.setShotMode(ShotMode.CLOSE_LOW), pidShotUtil));
    tarmacHigh = new JoystickButton(operator, 3);
    tarmacHigh.whenPressed(new InstantCommand(() -> pidShotUtil.setShotMode(ShotMode.TARMAC_HIGH), pidShotUtil));
    // dynamicShot = new JoystickButton(operator, Button.kLeftBumper.value);
    // dynamicShot.whileHeld(new DynamicShot(pidShotUtil, visionUtil));
    // Run Shot
    toggleShotIntent = new JoystickButton(operator, 4);
    toggleShotIntent.whenPressed(new InstantCommand(() -> setIsShooting(!isShooting)));
    // Run Intake
    toggleIntakeIntent = new JoystickButton(operator, 5);
    toggleIntakeIntent.whenPressed(new InstantCommand(() -> magUtil.toggleCollectingIntent(), magUtil));

  }

  private void setSubsystemDefaultCommands() {
    driveUtil.setDefaultCommand(new RunCommand(() -> driveUtil.driveRobot(), driveUtil));
    magUtil.setDefaultCommand(new OperateMagazine(magUtil));
  }

  private void setupAutoSendable() {
    autoChooser = new SendableChooser<Command>();
    autoChooser.addOption("Taxi", new AutoExit(driveUtil));
    // autoChooser.addOption("AutoSpecialTen", new AutoSpecialTen(driveUtil, pidShotUtil, visionUtil, magUtil));
    // autoChooser.addOption("AutoLowRight", new AutoLowRight(driveUtil, pidShotUtil, visionUtil, magUtil));
    // autoChooser.addOption("AutoScoreOne", new AutoScoreOne(driveUtil, magUtil, pidShotUtil));
    autoChooser.setDefaultOption("LowLeft", new AutoLowLeft(driveUtil, pidShotUtil, visionUtil, magUtil));
    autoChooser.addOption("Left", new AutoGyroTenLeft(driveUtil, pidShotUtil, visionUtil, magUtil));
    autoChooser.addOption("Middle", new AutoGyroTenMid(driveUtil, pidShotUtil, visionUtil, magUtil));
    autoChooser.addOption("Right", new AutoGyroTenRight(driveUtil, pidShotUtil, visionUtil, magUtil));

  }

  private void setupDriveSendable(){
    driveMode = new SendableChooser<DriveMode>();
    driveMode.addOption("Xbox", DriveMode.XBOX);
    driveMode.addOption("Tank", DriveMode.J_TANK);
    driveMode.addOption("Arcade", DriveMode.J_ARCADE);
    driveMode.setDefaultOption("MArcade", DriveMode.J_MARCADE);
  }

  private void setupShuffleboard() {
    // TAB DEFINITIONS
    driverTab = Shuffleboard.getTab("TELEOPERATED");
    sensorTab = Shuffleboard.getTab("SENSORS");

    // AUTONOMOUS SENDABLECHOOSER
    driverTab.add("Autonomous Command", autoChooser).withWidget(BuiltInWidgets.kComboBoxChooser).withSize(2, 1).withPosition(0, 0);
    driverTab.add("Drive Mode", driveMode).withWidget(BuiltInWidgets.kComboBoxChooser).withSize(1, 1).withPosition(0, 2);

    // SUBSYSTEM INIT SEQUENCES
    // driveUtil.initShuffleboard(driverTab, sensorTab);
    magUtil.initShuffleboard(driverTab, sensorTab);
    climbUtil.initShuffleboard(driverTab, sensorTab);
    visionUtil.initShuffleboard(driverTab, sensorTab);
    pidShotUtil.initShuffleboard(driverTab, sensorTab);
  }

  // Drive Mode
  public static DriveMode getDriveMode() {
    return driveMode.getSelected();
  }

  /*
   * Controller get() methods - returns numerical value of axis
   */
  // Joystick
  public static double getRealLeftJoystickY() {
    return leftJoystick.getY();
  }
  public static double getRealRightJoystickY() {
    return rightJoystick.getY();
  }

  public static double getRealLeftJoystickZ() {
    return leftJoystick.getZ();
  }
  public static double getRealRightJoystickZ() {
    return rightJoystick.getZ();
  }

  public static double getRealLeftJoystickX() {
    return leftJoystick.getX();
  }
  public static double getRealRightJoystickX() {
    return rightJoystick.getX();
  }

  // Driver controller
  public static double getDriverLeftTrigger() {
    return driver.getLeftTriggerAxis();
  }

  public static double getDriverRightTrigger() {
    return driver.getRightTriggerAxis();
  }

  public static double getDriverLeftJoystickX() {
    return driver.getLeftX();
  }

  public static double getDriverLeftJoystickY() {
    return driver.getLeftY();
  }

  public static double getDriverRightJoystickX() {
    return driver.getRightX();
  }

  public static double getDriverRightJoystickY() {
    return driver.getRightY();
  }

  // Operator controller
  public static double getOperatorLeftTrigger() {
    return operator.getRawAxis(2);
  }

  public static double getOperatorRightTrigger() {
    return operator.getRawAxis(3);

  }


  public static double getOperatorLeftJoystickX() {
    return operator.getRawAxis(0);
  }

  public static double getOperatorLeftJoystickY() {
    return operator.getRawAxis(1);
  }

  public static double getOperatorRightJoystickX() {
    return operator.getRawAxis(4);
  }

  public static double getOperatorRightJoystickY() {
    return operator.getRawAxis(5);
  }

  public static double getPOV() {
    return operator.getPOV();
  }


  // STATE BOOLEANS
  // isCollecting
  public static void setCollecting(boolean value) {
    isCollecting = value;
  }

  public static boolean isCollecting() {
    return isCollecting;
  }

  // isShooting
  public static void setIsShooting(boolean value) {
    isShooting = value;
  }

  public static boolean isShooting() {
    return isShooting;
  }

  // isShotReady
  public static void setShotReady(boolean value) {
    isShotReady = value;
  }

  public static boolean isShotReady() {
    return isShotReady;
  }

  // hasOuterBall
  public static void setOuterBall(boolean value) {
    hasOuterBall = value;
  }

  public static boolean hasOuterBall() {
    return hasOuterBall;
  }

  // hasInnerBall
  public static void setInnerBall(boolean value) {
    hasInnerBall = value;
  }

  public static boolean hasInnerBall() {
    return hasInnerBall;
  }

  // innerColor
  public static void setInnerColor(SkittleColor color) {
    innerColor = color;
  }

  public static SkittleColor getInnerColor() {
    return innerColor;
  }

  // innerColor
  public static void setOuterColor(SkittleColor color) {
    outerColor = color;
  }

  public static SkittleColor getOuterColor() {
    return outerColor;
  }

  // autoCargoTicks
  public static void setAutoCargoTicks(double ticks) {
    autoCargoTicks = ticks;
  }

  public static double getAutoCargoTicks() {
    return autoCargoTicks;
  }

  // tiltProtect
  public static void setTiltProtect(boolean b) {
    tiltProtect = b;
  }

  public static boolean getTiltProtect() {
    return tiltProtect;
  }

  // LEDs
  // LED mode
  public void setLEDMode(LEDMode newLedMode) {
    ledUtil.setLEDMode(newLedMode);
  }
  
  // Motor mode
  public void setDriveNeutralMode(NeutralMode mode){
    driveUtil.setDriveNeutralMode(mode);
  }

  public void setPistons(ClimbState climbState) {
    climbUtil.setClimbState(climbState);
  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    return autoChooser.getSelected();
  }

}
