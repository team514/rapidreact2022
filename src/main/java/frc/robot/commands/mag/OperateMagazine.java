// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.mag;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.RobotContainer;
import frc.robot.subsystems.MagUtil;

public class OperateMagazine extends CommandBase {

  MagUtil magUtil;

  /** Creates a new OperateIntake. */
  public OperateMagazine(MagUtil magUtil) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.magUtil = magUtil;
    addRequirements(magUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  /**
   * Inner index acted as the brush intake motor
   * Outer index acted correctly
   * Brush intake acted as the inner index
   * Index motors were running very slow
   */

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (RobotContainer.hasOuterBall() && RobotContainer.hasInnerBall()) {
      // State 1 - Have two balls, can't intake, can shoot
      magUtil.setBrushMotorSpeed(0.0);
      if (RobotContainer.isShooting() && RobotContainer.isShotReady()) {
        magUtil.setOuterIndexSpeed(Constants.indexMotorSpeed);
        magUtil.setInnerIndexSpeed(Constants.indexMotorSpeed);
      } else {
        magUtil.setOuterIndexSpeed(0.0);
        magUtil.setInnerIndexSpeed(0.0);
      }
    } else if (RobotContainer.hasOuterBall() && !RobotContainer.hasInnerBall()) {
      // State 2 - Have only outer ball, can intake, can't shoot
      if (RobotContainer.isCollecting()) {
        magUtil.setBrushMotorSpeed(Constants.brushMotorSpeed);
      } else {
        magUtil.setBrushMotorSpeed(0.0);
      }
      magUtil.setOuterIndexSpeed(Constants.indexMotorSpeed);
      magUtil.setInnerIndexSpeed(Constants.indexMotorSpeed);
    } else if (!RobotContainer.hasOuterBall() && RobotContainer.hasInnerBall()) {
      // State 3 - Have only inner ball, can intake, can shoot
      if (RobotContainer.isCollecting()) {
        magUtil.setBrushMotorSpeed(Constants.brushMotorSpeed);
        magUtil.setOuterIndexSpeed(Constants.indexMotorSpeed);
      } else {
        magUtil.setBrushMotorSpeed(0.0);
        magUtil.setOuterIndexSpeed(0.0);
      }
      if (RobotContainer.isShooting() && RobotContainer.isShotReady()) {
        magUtil.setInnerIndexSpeed(Constants.indexMotorSpeed);
      } else {
        magUtil.setInnerIndexSpeed(0.0);
      }
    } else {
      // State 4 - Have no balls, can intake, can't shoot
      if (RobotContainer.isCollecting()) {
        magUtil.setBrushMotorSpeed(Constants.brushMotorSpeed);
        magUtil.setOuterIndexSpeed(Constants.indexMotorSpeed);
      } else {
        magUtil.setBrushMotorSpeed(0.0);
        magUtil.setOuterIndexSpeed(0.0);
      }
      magUtil.setInnerIndexSpeed(0.0);
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
