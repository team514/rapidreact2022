// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.shot;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.handlers.ShotMode;
import frc.robot.subsystems.PIDShotUtil;
import frc.robot.subsystems.VisionUtil;

public class DynamicShot extends CommandBase {

  private PIDShotUtil pidShotUtil;
  private VisionUtil visionUtil;
  private double rpm;

  /** Creates a new DynamicShot. */
  public DynamicShot(PIDShotUtil pidShotUtil, VisionUtil visionUtil) {

    this.pidShotUtil = pidShotUtil;
    this.visionUtil = visionUtil;

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(pidShotUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    pidShotUtil.setShotMode(ShotMode.DYNAMIC);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    rpm = visionUtil.getDynamicRPM();
    pidShotUtil.setSpeed(rpm);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    pidShotUtil.setShotMode(ShotMode.OFF);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
