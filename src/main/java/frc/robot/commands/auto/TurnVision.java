// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.DriveUtil;
import frc.robot.subsystems.VisionUtil;

public class TurnVision extends CommandBase {

  private DriveUtil driveUtil;
  private VisionUtil visionUtil;
  private boolean right, done;
  private double tx;

  /** Creates a new TurnVision. */
  public TurnVision(DriveUtil driveUtil, VisionUtil visionUtil, boolean right) {

    this.driveUtil = driveUtil;
    this.visionUtil = visionUtil;
    this.right = right;

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(driveUtil, visionUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    done = false;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (right) {
      driveUtil.tankDrive(Constants.autoPivotSpeed, -Constants.autoPivotSpeed);
    } else {
      driveUtil.tankDrive(-Constants.autoPivotSpeed, Constants.autoPivotSpeed);
    }

    if (visionUtil.hasTarget()) {
      tx = visionUtil.getTargetX();
      if (tx >= -Constants.angleDeadBandRange && tx <= Constants.angleDeadBandRange) {
        driveUtil.tankDrive(0, 0);
        done = true;
      }
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}
