// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.RobotContainer;
import frc.robot.handlers.ShotMode;
import frc.robot.subsystems.MagUtil;
import frc.robot.subsystems.PIDShotUtil;

public class Shoot extends CommandBase {
  
  private boolean done;
  private PIDShotUtil pidShotUtil;
  private MagUtil magUtil;
  private int countCPU;

  /** Creates a new Shoot. */
  public Shoot(PIDShotUtil pidShotUtil, MagUtil magUtil) {

    this.pidShotUtil = pidShotUtil;
    this.magUtil = magUtil;

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(pidShotUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    done = false;
    RobotContainer.setIsShooting(true);
    countCPU = 0;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if(!magUtil.getOuterBanner() && !magUtil.getInnerBanner() && pidShotUtil.upToSpeed()) {
      if(countCPU++ >= 30) {
        pidShotUtil.setShotMode(ShotMode.OFF);
        done = true;
      }
    } else {
      countCPU = 0;
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}
