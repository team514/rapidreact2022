// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto.groups;

import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.auto.DriveDistance;
import frc.robot.commands.auto.DriveToWall;
import frc.robot.commands.auto.Shoot;
import frc.robot.handlers.ShotMode;
import frc.robot.subsystems.DriveUtil;
import frc.robot.subsystems.MagUtil;
import frc.robot.subsystems.PIDShotUtil;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class AutoScoreOne extends SequentialCommandGroup {
  /** Creates a new AutoScoreOne. */
  public AutoScoreOne(DriveUtil driveUtil, MagUtil magUtil, PIDShotUtil pidShotUtil) {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new InstantCommand(() -> pidShotUtil.setShotMode(ShotMode.CLOSE_LOW), pidShotUtil), // Start shot motors
      new Shoot(pidShotUtil, magUtil),
      new DriveToWall(driveUtil, false, false)
    );
  }
}
