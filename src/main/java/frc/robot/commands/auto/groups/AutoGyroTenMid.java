// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto.groups;

import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.Constants;
import frc.robot.RobotContainer;
import frc.robot.commands.auto.DriveFromCargo;
import frc.robot.commands.auto.DriveToCargo;
import frc.robot.commands.auto.Shoot;
import frc.robot.commands.auto.TimerWait;
import frc.robot.commands.auto.TurnAngle;
import frc.robot.handlers.ShotMode;
import frc.robot.subsystems.DriveUtil;
import frc.robot.subsystems.MagUtil;
import frc.robot.subsystems.PIDShotUtil;
import frc.robot.subsystems.VisionUtil;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class AutoGyroTenMid extends SequentialCommandGroup {
  /** Creates a new AutoSpecialTen. */
  public AutoGyroTenMid(DriveUtil driveUtil, PIDShotUtil pidShotUtil, VisionUtil visionUtil, MagUtil magUtil) {
    // Add your commands in the addCommands() call, e.g.
    // addCommands(new FooCommand(), new BarCommand());
    addCommands(
      new InstantCommand(() -> RobotContainer.setCollecting(true)), // Enable intake
      new ParallelRaceGroup(
        new DriveToCargo(driveUtil),
        new TimerWait(5.0)
      ),
       // Drive straight, out of tarmac
      new InstantCommand(() -> pidShotUtil.setShotMode(ShotMode.AUTO_HIGH), pidShotUtil), // Start shot motors
      new DriveFromCargo(driveUtil), // Drive back to shooting position
      new TurnAngle(driveUtil, Constants.autoTenMidGyroValue),
      new Shoot(pidShotUtil, magUtil)); // Shoot both balls
  }
}
