// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto.demo;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.handlers.LimelightMode;
import frc.robot.subsystems.DriveUtil;
import frc.robot.subsystems.VisionUtil;

public class AutoFollow extends CommandBase {
  private VisionUtil visionUtil;
  private DriveUtil driveUtil;

  private double tx, ta;

  /** Creates a new AutoFollow. */
  public AutoFollow(VisionUtil visionUtil, DriveUtil driveUtil) {
    addRequirements(visionUtil, driveUtil);
    this.visionUtil = visionUtil;
    this.driveUtil = driveUtil;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    visionUtil.setLimelightMode(LimelightMode.VISION_PROCESSOR);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    tx = visionUtil.getTargetX();
    // ty = visionUtil.getTargetY();
    ta = visionUtil.getTargetArea();

    if (!visionUtil.hasTarget()) {
      driveUtil.tankDrive(0, 0);
      return;
    }

    boolean turnLeft = tx <= -Constants.angleDeadBandRange;
    boolean turnRight = tx >= Constants.angleDeadBandRange;
    boolean driveForward = ta < Constants.targetArea - Constants.distanceDeadBandRange;
    boolean driveBackward = ta > Constants.targetArea + Constants.distanceDeadBandRange;

    boolean turning = turnLeft || turnRight;
    boolean driving = driveForward || driveBackward;


    if (turning && driving) { // Auto Turn and Drive
      if (turnLeft && driveForward) {
        driveUtil.tankDrive(Constants.autoSlowTurnSpeed, Constants.autoFastTurnSpeed);
      } else if (turnLeft && driveBackward) {
        driveUtil.tankDrive(-Constants.autoFastTurnSpeed, -Constants.autoSlowTurnSpeed);
      } else if (turnRight && driveForward){
        driveUtil.tankDrive(Constants.autoFastTurnSpeed, Constants.autoSlowTurnSpeed);
      } else if (turnRight && driveBackward){
        driveUtil.tankDrive(-Constants.autoSlowTurnSpeed, -Constants.autoFastTurnSpeed);
      }

    } else if (turning && !driving) { // Auto Turn
      if (turnLeft) {
        driveUtil.tankDrive(-Constants.autoPivotSpeed, Constants.autoPivotSpeed);
      } else if (turnRight) {
        driveUtil.tankDrive(Constants.autoPivotSpeed, -Constants.autoPivotSpeed);
      }
    } else if (!turning && driving) { // Auto Drive
      if (driveForward) { // If Target is far, go forward
        driveUtil.tankDrive(Constants.autoDriveSpeed, Constants.autoDriveSpeed);
      } else if (driveBackward) {
        driveUtil.tankDrive(-Constants.autoDriveSpeed, -Constants.autoDriveSpeed);
      }

    } else { // No Turn or Drive
      driveUtil.tankDrive(0, 0);
    }

  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    visionUtil.setLimelightMode(LimelightMode.DRIVER_CAMERA);
    driveUtil.tankDrive(0, 0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
