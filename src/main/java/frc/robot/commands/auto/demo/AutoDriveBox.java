// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto.demo;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.Constants;
import frc.robot.commands.auto.DriveDistance;
import frc.robot.commands.auto.TurnAngle;
import frc.robot.subsystems.DriveUtil;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class AutoDriveBox extends SequentialCommandGroup {
  /** Creates a new AutoDriveBox. */
  public AutoDriveBox(DriveUtil driveUtil) {
    addCommands(
      new DriveDistance(driveUtil, 3*Constants.tickfeet),
      new TurnAngle(driveUtil, 90),
      new DriveDistance(driveUtil, 3*Constants.tickfeet),
      new TurnAngle(driveUtil, 90),
      new DriveDistance(driveUtil, 3*Constants.tickfeet),
      new TurnAngle(driveUtil, 90),
      new DriveDistance(driveUtil, 3*Constants.tickfeet),
      new TurnAngle(driveUtil, 90)
    );
  }
}
