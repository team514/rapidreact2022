// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.RobotContainer;
import frc.robot.subsystems.DriveUtil;

public class DriveToCargo extends CommandBase {
  
  private DriveUtil driveUtil;
  private boolean done;

  /** Creates a new DriveToCargo. */
  public DriveToCargo(DriveUtil driveUtil) {

    this.driveUtil = driveUtil;

    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(driveUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    done = false;
    driveUtil.resetEncoders();
    driveUtil.resetGyro();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (RobotContainer.hasInnerBall() && RobotContainer.hasOuterBall()) {
      driveUtil.tankDrive(0, 0);
      done = true;
      return;
    }
    driveUtil.autoStraight(Constants.autoDriveSpeed);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    RobotContainer.setAutoCargoTicks(driveUtil.getLeftEncoder());
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}
