// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.RobotContainer;
import frc.robot.subsystems.DriveUtil;

public class DriveFromCargo extends CommandBase {

  private DriveUtil driveUtil;
  private double targetFeet, targetTicks;
  private boolean forward;
  private boolean done;
  double currentAngle, coerceValue;

  /** Creates a new DriveDistance. */
  public DriveFromCargo(DriveUtil driveUtil) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.driveUtil = driveUtil;
    addRequirements(this.driveUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    done = false;
    driveUtil.resetEncoders();
    driveUtil.resetGyro();
    targetTicks = RobotContainer.getAutoCargoTicks() * 0.8;
    if (targetTicks < 0) {
      forward = false;
    } else {
      forward = true;
    }
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    // If the value of the encoder is greater than what we are targeting, stop and end command.
    if (Math.abs(driveUtil.getLeftEncoder()) >= Math.abs(targetTicks)) {
      driveUtil.tankDrive(0, 0);
      done = true;
      return;
    }
    // If we want to drive forward, drive at a positive speed. Otherwise, drive at a negative speed.
    if (forward) {
      driveUtil.autoStraight(Constants.autoDriveSpeed);
    } else {
      driveUtil.autoStraight(-Constants.autoDriveSpeed);
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    driveUtil.tankDrive(0, 0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}
