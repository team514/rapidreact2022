// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.auto;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.DriveUtil;

public class DriveToWall extends CommandBase {

  private double lastTicks, newTicks;
  private int countNoChange;
  private DriveUtil driveUtil;
  boolean done, left, forward;

  /** Creates a new DriveToWall. */
  public DriveToWall(DriveUtil driveUtil, boolean left, boolean forward) {
    this.driveUtil = driveUtil;
    this.left = left;
    this.forward = forward;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(driveUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    driveUtil.resetGyro();
    driveUtil.resetEncoders();
    lastTicks = getEncoder();
    done = false;
  }

  public double getEncoder() {
    if (left) {
      return driveUtil.getLeftEncoder();
    } else {
      return driveUtil.getRightEncoder();
    }
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    newTicks = getEncoder();
    if (Math.abs(newTicks - lastTicks) < 5) {
      countNoChange++;
    } else {
      countNoChange = 0;
    }
    if(countNoChange >= 60) {
      done = true;
      driveUtil.tankDrive(0, 0);
      return;
    }
    lastTicks = getEncoder();
    if (forward) {
      driveUtil.autoStraight(Constants.autoFastDriveSpeed);
    } else {
      driveUtil.autoStraight(-Constants.autoFastDriveSpeed);
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    driveUtil.tankDrive(0, 0);
    done = true;
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}
