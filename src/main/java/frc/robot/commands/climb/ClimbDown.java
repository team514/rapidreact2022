// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.climb;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ClimbUtil;

public class ClimbDown extends CommandBase {
  
  private ClimbUtil climbUtil;
  private boolean done;

  /** Creates a new ClimbDown. */
  public ClimbDown(ClimbUtil climbUtil) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(climbUtil);
    this.climbUtil = climbUtil;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    done = false;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    climbUtil.climbDown();
    done = true;
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}
