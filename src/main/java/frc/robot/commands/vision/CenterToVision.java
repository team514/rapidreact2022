// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.vision;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.DriveUtil;
import frc.robot.subsystems.VisionUtil;

public class CenterToVision extends CommandBase {
  private VisionUtil visionUtil;
  private DriveUtil driveUtil;
  private boolean done;
  private double tx;

  /** Creates a new AutoFollow. */
  public CenterToVision(VisionUtil visionUtil, DriveUtil driveUtil) {
    addRequirements(visionUtil, driveUtil);
    this.visionUtil = visionUtil;
    this.driveUtil = driveUtil;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    // visionUtil.setLimelightMode(LimelightMode.VISION_PROCESSOR);
    done = false;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    tx = visionUtil.getTargetX();

    if (!visionUtil.hasTarget()) {
      driveUtil.tankDrive(0, 0);
      done = true;
      return;
    }

    boolean turnLeft = tx <= -Constants.angleDeadBandRange;
    boolean turnRight = tx >= Constants.angleDeadBandRange;

    if (turnLeft) {
      driveUtil.tankDrive(-Constants.autoPivotSpeed, Constants.autoPivotSpeed);
    } else if (turnRight) {
      driveUtil.tankDrive(Constants.autoPivotSpeed, -Constants.autoPivotSpeed);
    } else { // Stop Robot
      driveUtil.tankDrive(0, 0);
      done = true;
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    // visionUtil.setLimelightMode(LimelightMode.DRIVER_CAMERA);
    driveUtil.tankDrive(0, 0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}
