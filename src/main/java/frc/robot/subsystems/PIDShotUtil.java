// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import java.util.Map;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel;
import com.revrobotics.RelativeEncoder;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInLayouts;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInWidgets;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardLayout;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj2.command.PIDSubsystem;
import frc.robot.Constants;
import frc.robot.RobotContainer;
import frc.robot.handlers.ShotMode;

public class PIDShotUtil extends PIDSubsystem {

  // HARDWARE
  private CANSparkMax primaryShot, secondaryShot;
  private RelativeEncoder primaryEncoder, secondaryEncoder;

  // VARIABLES
  private ShotMode shotMode;
  private double shotTargetRPM = 0.0; // Initial shot speed should be zero
  private boolean upToSpeed = false;

  // SHUFFLEBOARD
  ShuffleboardLayout shotUtilData, rpmGrid;
  NetworkTableEntry shotRPM, targetRpmDIAG, targetRpmDS, shotRPMGraph, shotModeDIAG, shotModeDS, shotReadyDIAG, shotReadyDS, autoShotSpeed;

  /** Creates a new PIDShotUtil. */
  public PIDShotUtil() {
    super(new PIDController(0.00027, 0.0006022304833, 0)); // The PIDController used by the subsystem

    primaryShot = new CANSparkMax(Constants.primaryShot, CANSparkMaxLowLevel.MotorType.kBrushless);
    secondaryShot = new CANSparkMax(Constants.secondaryShot, CANSparkMaxLowLevel.MotorType.kBrushless);
    primaryEncoder = primaryShot.getEncoder();
    secondaryEncoder = secondaryShot.getEncoder();
    secondaryShot.follow(primaryShot, true);

    primaryShot.setIdleMode(IdleMode.kCoast);
    secondaryShot.setIdleMode(IdleMode.kCoast);

    getController().setTolerance(30, 8 / 0.02);
    // 30 is our positionTolerance, and (10/0.02) is our velocityTolerance
    // Since our PID is a velocity loop (RPM), these tolerances are actually the
    // velocityTolerance and accelerationTolerance, respectively
    // 30 is how far away from our setpoint we can be
    // 10 is how "fast" our velocity can be changing per period
    // 0.02 is our period value - 50 calculations per second (1/0.02)
    // Essentially...
    // - for the first value, our RPM must be at the setpoint plus or minus 30
    // - for the second value, our acceleration must be less than 500 RPM/s (the RPM
    // curve must be flat, not steep)
    // - if both of the above are true, we are "at our setpoint"
    // We can print our velocityError to the dashboard for testing to see what a
    // "reasonable" value is - getController().getVelocityError()
    // I will explain this in-depth on Thursday
    // More info:
    // https://docs.wpilib.org/en/stable/docs/software/advanced-controls/controllers/pidcontroller.html#specifying-and-checking-tolerances

    setSetpoint(0);
    setShotMode(ShotMode.OFF);
  }

  // STATE LOGIC
  // Sets the shot mode, then decides whether to run the motors or not
  public void setShotMode(ShotMode newShotMode) {
    shotTargetRPM = newShotMode.getTargetRPM();
    if (newShotMode.equals(ShotMode.OFF)) {
      disable();
      setSetpoint(0);
      RobotContainer.setIsShooting(false);
    } else {
      if (!isEnabled()) {
        enable();
      }
      if (!newShotMode.equals(ShotMode.DYNAMIC)) {
        getController().setTolerance(30, 8 / 0.02);
        setSetpoint(shotTargetRPM);
      } else {
        getController().setTolerance(100, 20 / 0.02);
      }
    }
    shotMode = newShotMode;
  }

  public void setSpeed(double rpm) {
    setSetpoint(rpm);
    shotTargetRPM = rpm;
  }

  // Returns enumerated form of ShotMode
  public ShotMode getShotMode() {
    return shotMode;
  }

  // Checks if shot motor is up to speed, if setpoint is 0 then it's always false
  public boolean upToSpeed() {
    if (shotTargetRPM != 0) {
      upToSpeed = getController().atSetpoint();
    } else {
      upToSpeed = false;
    }
    return upToSpeed;
  }

  // Changes if the operator wants the robot to shoot.
  public void toggleShootingIntent() {
    RobotContainer.setIsShooting(!RobotContainer.isShooting());
  }

  // SENSORS
  // Gets primary shot motor live RPM
  public double getPrimaryRPM() {
    return primaryEncoder.getVelocity();
  }

  // Gets secondary shot motor live RPM
  public double getSecondaryRPM() {
    return secondaryEncoder.getVelocity();
  }

  // set shot rpm to dashboard slider (during auto)
  public void setShotToDashboardSlider() {
    setSetpoint(autoShotSpeed.getDouble(0));
  }

  @Override
  public void useOutput(double output, double setpoint) {
    // Use the output here
    if(output < 0) {
      output = 0.1;
    }
    primaryShot.set(output);
  }

  @Override
  public double getMeasurement() {
    // Return the process variable measurement here
    return getPrimaryRPM();
  }

  // SHUFFLEBOARD
  public void initShuffleboard(ShuffleboardTab driverTab, ShuffleboardTab sensorTab) {
    // LAYOUTS
    shotUtilData = sensorTab.getLayout("SHOTUTIL DATA", BuiltInLayouts.kList).withSize(3, 5).withPosition(6, 0);
    rpmGrid = shotUtilData.getLayout("RPM Values", BuiltInLayouts.kGrid)
        .withProperties(Map.of("Number of columns", 2, "Number of rows", 1));

    // // VALUES
    // shotRPM = rpmGrid.add("Current RPM", 0).withWidget(BuiltInWidgets.kTextView).getEntry();
    // targetRpmDIAG = rpmGrid.add("Target RPM", 0).withWidget(BuiltInWidgets.kTextView).getEntry();
    // targetRpmDS = driverTab.add("Target RPM", 0).withWidget(BuiltInWidgets.kTextView).withSize(1, 1).withPosition(1, 3).getEntry();
    // shotModeDIAG = shotUtilData.add("Shot Mode", "NULL").withWidget(BuiltInWidgets.kTextView).getEntry();
    // shotReadyDS = driverTab.add("Shot Ready", false).withWidget(BuiltInWidgets.kBooleanBox).withSize(1, 1).withPosition(1, 2).getEntry();
    // shotReadyDIAG = shotUtilData.add("Shot Ready", false).withWidget(BuiltInWidgets.kBooleanBox).getEntry();
    // shotModeDS = driverTab.add("Shot Mode", "NULL").withWidget(BuiltInWidgets.kTextView).withSize(1, 1).withPosition(0, 3).getEntry();
    // // autoShotSpeed = driverTab.add("Auto Shot Speed", Constants.tarmacHighRPM).withWidget(BuiltInWidgets.kNumberSlider).withProperties(Map.of("Min", 0, "Max", 5400)).withSize(3, 1).withPosition(0, 5).getEntry();
    // // DISABLED FOR PERFORMANCE REASONS:
    // // shotRPMGraph = shotUtilData.add("Shot RPM", 0).withWidget(BuiltInWidgets.kGraph).withProperties(Map.of("Visible time", 15)).getEntry();
  }

  public void updateShuffleboard() {
    // targetRpmDIAG.setDouble(getController().getSetpoint());
    // shotRPM.setDouble(getPrimaryRPM());
    // // SEE initShuffleboard:
    // // shotRPMGraph.setDouble(getPrimaryRPM());
    // shotModeDIAG.setString(getShotMode().name());
    // shotReadyDIAG.setBoolean(upToSpeed());
    // shotReadyDS.setBoolean(upToSpeed());
    // targetRpmDS.setDouble(getController().getSetpoint());
    // shotModeDS.setString(getShotMode().name());
  }

  @Override
  public void periodic() {
    super.periodic();
    RobotContainer.setShotReady(upToSpeed());
    updateShuffleboard();
  }
}
