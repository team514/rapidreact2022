// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import java.util.Map;

import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInLayouts;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInWidgets;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardLayout;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.RobotContainer;

public class MagUtil extends SubsystemBase {

  private WPI_VictorSPX brushMotor, outerIndex, innerIndex;
  private DigitalInput outerBanner, innerBanner;
  // private ColorSensorV3 innerColor, outerColor;

  // private final ColorMatch colorMatch = new ColorMatch();
  // public final Color blueTarget = new Color(0.143, 0.427, 0.429);
  // public final Color redTarget = new Color(0.561, 0.232, 0.114);
  // public final Color whiteTarget = new Color(0.273, 0.500, 0.219);

  // COLOR MATCHING
  // SkittleColor matchedColor;
  // Color color;
  // ColorMatchResult match;
  // boolean matchesAlliance;

  // SHUFFLEBOARD
  ShuffleboardLayout magUtilData, bannerSensorLayout, colorSensorLayout, collectingBooleans;
  NetworkTableEntry outerBannerNT, innerBannerNT, outerColorNT, innerColorNT, ballMatchesAllianceDIAG,
      ballMatchesAllianceDS, intakeStatus, shootingIntentDIAG, shootingIntentDS;

  /** Creates a new MagUtil. */
  public MagUtil() {
    brushMotor = new WPI_VictorSPX(Constants.brushMotor);
    outerIndex = new WPI_VictorSPX(Constants.outerIndex);
    innerIndex = new WPI_VictorSPX(Constants.innerIndex);

    brushMotor.setInverted(true);
    outerIndex.setInverted(true);
    innerIndex.setInverted(true);

    outerBanner = new DigitalInput(Constants.outerBanner);
    innerBanner = new DigitalInput(Constants.innerBanner);

    // innerColor = new ColorSensorV3(I2C.Port.kOnboard);
    // outerColor = new ColorSensorV3(I2C.Port.kMXP);

    // colorMatch.addColorMatch(blueTarget);
    // colorMatch.addColorMatch(redTarget);
    // colorMatch.addColorMatch(whiteTarget);
  }

  // STATE LOGIC
  // Changes if the operator wants the robot to collect.
  public void toggleCollectingIntent() {
    RobotContainer.setCollecting(!RobotContainer.isCollecting());
  }

  public boolean getCollectingIntent() {
    return RobotContainer.isCollecting();
  }

  // MOTORS
  // Sets intake brush speed
  public void setBrushMotorSpeed(double speed) {
    brushMotor.set(speed);
  }

  // Sets outer index motor (closest to intake) speed
  public void setOuterIndexSpeed(double speed) {
    outerIndex.set(speed);
  }

  // Sets inner index motor (closest to shot) speed
  public void setInnerIndexSpeed(double speed) {
    innerIndex.set(speed);
  }

  // SENSORS
  // Banner:
  // Gets outer banner state (closest to intake)
  public boolean getOuterBanner() {
    return !outerBanner.get();
  }

  // Gets inner banner state (closest to shot)
  public boolean getInnerBanner() {
    return !innerBanner.get();
  }

  // Color:
  // Returns enumerated form of color matched
  // public SkittleColor getSkittleColor(ColorSensorV3 colorSensor) {
  //   color = colorSensor.getColor();
  //   match = colorMatch.matchClosestColor(color);

  //   if (match.color == blueTarget) {
  //     matchedColor = SkittleColor.BLUE;
  //   } else if (match.color == redTarget) {
  //     matchedColor = SkittleColor.RED;
  //   } else {
  //     matchedColor = SkittleColor.OTHER;
  //   }
  //   return matchedColor;
  // }

  // // Boolean that states whether inner ball matches alliance color
  // public boolean ballMatchesAlliance() {
  //   matchesAlliance = false;
  //   if (DriverStation.getAlliance() == Alliance.Blue) {
  //     if (getSkittleColor(innerColor) == SkittleColor.BLUE) {
  //       matchesAlliance = true;
  //     } else {
  //       matchesAlliance = false;
  //     }
  //   }
  //   if (DriverStation.getAlliance() == Alliance.Red) {
  //     if (getSkittleColor(innerColor) == SkittleColor.RED) {
  //       matchesAlliance = true;
  //     } else {
  //       matchesAlliance = false;
  //     }
  //   }
  //   return matchesAlliance;
  // }

  // SHUFFLEBOARD
  public void initShuffleboard(ShuffleboardTab driverTab, ShuffleboardTab sensorTab) {
    // LAYOUTS
    // magUtilData = sensorTab.getLayout("MAGUTIL DATA", BuiltInLayouts.kList).withSize(3, 5).withPosition(0, 0);
    // bannerSensorLayout = magUtilData.getLayout("Banner Sensors", BuiltInLayouts.kGrid)
    //     .withProperties(Map.of("Number of columns", 2, "Number of rows", 1));
    // colorSensorLayout = magUtilData.getLayout("Color Sensors", BuiltInLayouts.kGrid)
    //     .withProperties(Map.of("Number of columns", 2, "Number of rows", 1));
    // collectingBooleans = magUtilData.getLayout("Collection Data", BuiltInLayouts.kGrid)
    //     .withProperties(Map.of("Number of columns", 2, "Number of rows", 1));

    // // VALUES
    // innerBannerNT = bannerSensorLayout.add("Inner", false).withWidget(BuiltInWidgets.kBooleanBox).getEntry();
    // outerBannerNT = bannerSensorLayout.add("Outer", false).withWidget(BuiltInWidgets.kBooleanBox).getEntry();
    // // innerColorNT = colorSensorLayout.add("Inner Color", "NULL").withWidget(BuiltInWidgets.kTextView).getEntry();
    // // outerColorNT = colorSensorLayout.add("Outer Color", "NULL").withWidget(BuiltInWidgets.kTextView).getEntry();
    // // ballMatchesAllianceDIAG = collectingBooleans.add("Alliance Ball", false).withWidget(BuiltInWidgets.kBooleanBox).getEntry();
    // // ballMatchesAllianceDS = driverTab.add("Alliance Ball", false).withWidget(BuiltInWidgets.kBooleanBox).withSize(1, 1).withPosition(0, 2).getEntry();
    // shootingIntentDIAG = collectingBooleans.add("Shooting", false).withWidget(BuiltInWidgets.kBooleanBox).getEntry();
    // shootingIntentDS = driverTab.add("Shooting", false).withWidget(BuiltInWidgets.kBooleanBox).withSize(2, 1).withPosition(0, 1).getEntry();
    // intakeStatus = driverTab.add("Collecting", false).withWidget(BuiltInWidgets.kBooleanBox).withSize(2, 1).withPosition(0, 4).getEntry();
  }

  public void updateShuffleboard() {
    // innerBannerNT.setBoolean(getInnerBanner());
    // outerBannerNT.setBoolean(getOuterBanner());
    // // innerColorNT.setString(getSkittleColor(innerColor).name());
    // // outerColorNT.setString(getSkittleColor(outerColor).name());
    // // ballMatchesAllianceDIAG.setBoolean(ballMatchesAlliance());
    // shootingIntentDIAG.setBoolean(RobotContainer.isShooting());
    // // ballMatchesAllianceDS.setBoolean(ballMatchesAlliance());
    // shootingIntentDS.setBoolean(RobotContainer.isShooting());
    // intakeStatus.setBoolean(RobotContainer.isCollecting());
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    updateShuffleboard();
    RobotContainer.setOuterBall(getOuterBanner());
    RobotContainer.setInnerBall(getInnerBanner());
    // RobotContainer.setInnerColor(getSkittleColor(innerColor));
    // RobotContainer.setOuterColor(getSkittleColor(outerColor));
  }
}
