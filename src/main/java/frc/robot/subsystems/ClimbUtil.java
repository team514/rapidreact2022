// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInLayouts;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInWidgets;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardLayout;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.handlers.ClimbState;

public class ClimbUtil extends SubsystemBase {

  private DoubleSolenoid leftClimbSolenoid, rightClimbSolenoid;
  private ClimbState climbState;

  // SHUFFLEBOARD
  ShuffleboardLayout climbUtilData;
  NetworkTableEntry pistonState;

  /** Creates a new ClimbUtil. */
  public ClimbUtil() {
 //   leftClimbSolenoid = new DoubleSolenoid(PneumaticsModuleType.CTREPCM, Constants.leftClimbForwardChannel,
 //       Constants.leftClimbReverseChannel);
 //   rightClimbSolenoid = new DoubleSolenoid(PneumaticsModuleType.CTREPCM, Constants.rightClimbForwardChannel,
 //       Constants.rightClimbReverseChannel);

    setClimbState(ClimbState.RETRACTED);
  }

  // CLIMB STATE LOGIC
  // Sets climb state within ClimbUtil
  public void setClimbState(ClimbState newClimbState){
    if (newClimbState != climbState){
      switch(newClimbState){
        case DEPLOYED:
          climbUp();
          break;
        case RETRACTED:
          climbDown();
          break;
      }
    }
    climbState = newClimbState;
  }

  // Returns current ClimbState enumeration
  public ClimbState getClimbState(){
    return climbState;
  }

  public void runClimb(ClimbState newNewClimbState){
    if (DriverStation.getMatchTime() <= 45){
      setClimbState(newNewClimbState);
    }
  }

  // Sets climbState to opposite of whatever it is
  public void toggleClimb() {
    switch (climbState){
      case DEPLOYED:
        setClimbState(ClimbState.RETRACTED);
        climbState = ClimbState.RETRACTED;
        break;
      case RETRACTED:
        setClimbState(ClimbState.DEPLOYED);
        climbState = ClimbState.DEPLOYED;
        break;
    }
  }

  // PISTON CONTROL
  // Deploys pistons
  public void climbUp() {
 //   leftClimbSolenoid.set(Constants.pistonExtended);
 //   rightClimbSolenoid.set(Constants.pistonExtended);
  }

  // Retracts pistons
  public void climbDown() {
 //   leftClimbSolenoid.set(Constants.pistonRetracted);
 //   rightClimbSolenoid.set(Constants.pistonRetracted);
  }

  // SHUFFLEBOARD
  public void initShuffleboard(ShuffleboardTab driverTab, ShuffleboardTab sensorTab){
    // LAYOUTS
    climbUtilData = sensorTab.getLayout("CLIMBUTIL DATA", BuiltInLayouts.kList).withSize(3, 1).withPosition(3, 0);
    
    // VALUES
    // pistonState = climbUtilData.add("Climb State", "NULL").withWidget(BuiltInWidgets.kTextView).getEntry();
  }

  public void updateShuffleboard(){
    // pistonState.setString(getClimbState().name());
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    updateShuffleboard();
  }
}
