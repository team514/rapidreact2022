// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.RobotContainer;
import frc.robot.handlers.SkittleColor;
import frc.robot.handlers.leds.LEDColor;
import frc.robot.handlers.leds.LEDMode;
import frc.robot.handlers.leds.LEDSegment;

public class LEDUtil extends SubsystemBase {
  AddressableLED ledStrip; // LED Strip
  AddressableLEDBuffer ledBuffer; // Buffer to store color values before writing
  boolean innerBanner, outerBanner; // Used later for conversion between sensor color and LED color
  LEDColor leftTopState, leftBottomState, rightTopState, rightBottomState, underState; // Store the current color of LEDs.
  LEDMode ledMode; // Store the current mode (routine) of the LEDs
  LEDColor topColor, bottomColor, allianceColor;

  int initialHue = 1;

  /** Creates a new LEDUtil. */
  public LEDUtil() {
    /*
     * LEDUtil explanation/purpose
     * This year, there are 4 LED strip segments, two of each on opposing sides of
     * the robot. These segments will be used to show the colors of the balls
     * currently in the robot's magazine instead of the driver and operator having
     * to look towards the dashboard during a match. As far as the code is
     * concerned, there is one strip, as for some reason the code refuses to run
     * with any more.
     */
    ledStrip = new AddressableLED(Constants.ledStripPWM);

    /*
     * Here we set up the lengths of the LEDBuffers. Since there are two segments to
     * each LED strip, the segment length has to be multiplied by the number of
     * segments to get the length of each full strip.
     */
    ledBuffer = new AddressableLEDBuffer((Constants.sideSegmentLength * Constants.amountOfSegments) + Constants.underSegmentLength);

    // Tells LED strip its length
    ledStrip.setLength(ledBuffer.getLength());

    // Start LED strip
    ledStrip.start();

    // Set LEDs to off
    setLEDMode(LEDMode.OFF);
  }

  public void setLEDMode(LEDMode newLedMode) {
    ledMode = newLedMode;
    switch (ledMode) {
      case RAINBOW:
        leftTopState = LEDColor.RAINBOW;
        leftBottomState = LEDColor.RAINBOW;
        rightTopState = LEDColor.RAINBOW;
        rightBottomState = LEDColor.RAINBOW;
        underState = LEDColor.RAINBOW;
      default:
        setLEDColor(LEDColor.OFF, LEDColor.OFF, LEDColor.OFF, LEDColor.OFF, LEDColor.OFF);
    }
  }

  // LED INTERACTIONS:
  // Write to LED buffer
  public void writeToBuffer(int bufferOffset, int length, int r, int g, int b) {
    for (var i = bufferOffset; i < length + bufferOffset; i++) {
      /*
       * This for loop iterates from the beginning of the bufferOffset, then while
       * the variable 'i' is less than however long the segment is, plus the
       * bufferOffset (to get the actual position on the strip), the loop runs,
       * and on end, it increases the value of 'i' by one.
       */
      ledBuffer.setRGB(i, r, g, b);
    }
  }

  // Set LED Color
  public void setLEDColor(LEDColor leftTopColor, LEDColor leftBottomColor,
      LEDColor rightTopColor,
      LEDColor rightBottomColor, LEDColor underColor) {
    // Write LED colors to buffer
    writeToBuffer(LEDSegment.LEFT_TOP.getBufferOffset(), Constants.sideSegmentLength, leftTopColor.getRed(), leftTopColor.getGreen(),
        leftTopColor.getBlue());
    writeToBuffer(LEDSegment.LEFT_BOTTOM.getBufferOffset(), Constants.sideSegmentLength, leftBottomColor.getRed(),
        leftBottomColor.getGreen(),
        leftBottomColor.getBlue());
    writeToBuffer(LEDSegment.RIGHT_TOP.getBufferOffset(), Constants.sideSegmentLength, rightTopColor.getRed(), rightTopColor.getGreen(),
        rightTopColor.getBlue());
    writeToBuffer(LEDSegment.RIGHT_BOTTOM.getBufferOffset(), Constants.sideSegmentLength, rightBottomColor.getRed(),
        rightBottomColor.getGreen(),
        rightBottomColor.getBlue());
    writeToBuffer(LEDSegment.UNDER.getBufferOffset(), Constants.underSegmentLength, underColor.getRed(), underColor.getGreen(),
        underColor.getBlue());

    // Store state values
    leftTopState = leftTopColor;
    leftBottomState = leftBottomColor;
    rightTopState = rightTopColor;
    rightBottomState = rightBottomColor;
    underState = allianceColor;

    // Publish the value of the buffer to the LEDs.
    ledStrip.setData(ledBuffer);
  }

  // LED MODES:
  // Set LED color based on ball color
  public void setToBallColor() {
    // Get color sensor values from RobotContainer
    innerBanner = RobotContainer.hasInnerBall();
    outerBanner = RobotContainer.hasOuterBall();
    
    switch (DriverStation.getAlliance()){
      case Red:
        allianceColor = LEDColor.RED;
        break;
      case Blue:
        allianceColor = LEDColor.BLUE;
        break;
      case Invalid:
      default:
        allianceColor = LEDColor.PURPLE;
        break;
    }

    if (innerBanner) {
      topColor = allianceColor;
    } else {
      topColor = LEDColor.PURPLE;
    }
    
    if (outerBanner) {
      bottomColor = allianceColor;
    } else {
      bottomColor = LEDColor.PURPLE;
    }

    // Only sets LED color if there is a change in state
    if (topColor != leftTopState || topColor != rightTopState || bottomColor != leftBottomState
        || bottomColor != rightBottomState || allianceColor != underState) {
      setLEDColor(topColor, bottomColor, topColor, bottomColor, allianceColor);
    }
  }

  // Rainbow idle mode
  private void rainbow() {
    // For every pixel
    for (var i = 0; i < ledBuffer.getLength(); i++) {
      // Calculate the hue - hue is easier for rainbows because the color
      // shape is a circle so only one value needs to precess
      final var hue = (initialHue + (i * 180 / ledBuffer.getLength())) % 180;
      // Set the value
      ledBuffer.setHSV(i, hue, 255, 128);
    }
    // Increase by to make the rainbow "move"
    initialHue += 4;
    // Check bounds
    initialHue %= 180;

    // Publish the value of the buffer to the LEDs.
    ledStrip.setData(ledBuffer);
  }

  @Override
  public void periodic() {
    /*
     * This method will be called once per scheduler run, updating the LED color
     * each time.
     */
    // setToBallColor();
    switch (ledMode) {
      case RAINBOW:
        rainbow();
        break;
      case BALL:
        setToBallColor();
        break;
      default:
        break;
    }
  }
}
