// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import java.util.Map;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInLayouts;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInWidgets;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardLayout;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.handlers.LimelightMode;

public class VisionUtil extends SubsystemBase {

  // Limelight
  LimelightMode limelightMode;
  NetworkTable limelightTwoTable;
  NetworkTableEntry taEntry, tyEntry, txEntry, tvEntry, ledModeEntry, camModeEntry, snapshotEntry;
  double x, area, degrees;
  boolean hasTarget, lights;

  // Dynamic Shot
  double dynamicRPM;
  double inputCenter, outputCenter;
  double scale, result;

  // SHUFFLEBOARD VARIABLES
  // VideoSource limelightTwoCamera =
  // CameraServer.getServer("limelight-two").getSource();
  ShuffleboardLayout limelightLayout, valuesLayout;
  NetworkTableEntry taBoard, txBoard, tyBoard, tvBoard, llMode, visionUtilCommand;

  /** Creates a new VisionUtil. */
  public VisionUtil() {
    limelightTwoTable = NetworkTableInstance.getDefault().getTable("limelight-two");
    txEntry = limelightTwoTable.getEntry("tx");
    tyEntry = limelightTwoTable.getEntry("ty");
    taEntry = limelightTwoTable.getEntry("ta");
    tvEntry = limelightTwoTable.getEntry("tv");
    snapshotEntry = limelightTwoTable.getEntry("snapshot");
    ledModeEntry = limelightTwoTable.getEntry("ledMode");
    camModeEntry = limelightTwoTable.getEntry("camMode");

    setLimelightMode(LimelightMode.VISION_PROCESSOR);
  }

  public double getDynamicRPM() {
    return coerce2Range(-tyEntry.getDouble(0), -11, 10.5, Constants.dynamicMinRPM, Constants.dynamicMaxRPM); // negate input angle to shoot faster when farther
  }

  // Calculates adjustment value based on input and bounds
  public double coerce2Range(double input, double inputMin, double inputMax, double outputMin, double outputMax) {
    /* Determine the center of the input range and output range */
    inputCenter = Math.abs(inputMax - inputMin) / 2 + inputMin;
    outputCenter = Math.abs(outputMax - outputMin) / 2 + outputMin;

    /* Scale the input range to the output range */
    scale = (outputMax - outputMin) / (inputMax - inputMin);

    /* Apply the transformation */
    result = (input - inputCenter) * scale + outputCenter;

    /* Constrain to the output range */
    return Math.max(Math.min(result, outputMax), outputMin);
  }

  // INTERACTING WITH LIMELIGHT
  public void setLimelightMode(LimelightMode newMode) {
    ledModeEntry.setNumber(newMode.getLedMode());
    camModeEntry.setNumber(newMode.getCamMode());
    limelightMode = newMode;
  }

  public LimelightMode getLimelightMode() {
    return limelightMode;
  }

  public double getTargetX() {
    return txEntry.getDouble(514);
  }

  public double getTargetY() {
    return tyEntry.getDouble(514);
  }

  public double getTargetArea() {
    return taEntry.getDouble(514);
  }

  public boolean hasTarget() {
    return tvEntry.getDouble(0) == 1;
  }

  public void takeSnapshot(){
    // snapshotEntry.setDouble(1);
    // if (snapshotEntry.getDouble(0) == 1){
    //   snapshotEntry.setDouble(0);
    // }
  }

  // SHUFFLEBOARD
  public void initShuffleboard(ShuffleboardTab driverTab, ShuffleboardTab sensorTab) {
    // CAMERAS
    // driverTab.add(limelightTwoCamera);

    // // LAYOUTS
    // limelightLayout = sensorTab.getLayout("LIMELIGHT DATA", BuiltInLayouts.kList).withSize(3, 4).withPosition(3, 1);
    // valuesLayout = limelightLayout.getLayout("Raw Values", BuiltInLayouts.kGrid)
    //     .withProperties(Map.of("Number of columns", 3, "Number of rows", 1));

    // // VALUES
    // txBoard = valuesLayout.add("Target X", 0).withWidget(BuiltInWidgets.kTextView).getEntry();
    // tyBoard = valuesLayout.add("Target Y", 0).withWidget(BuiltInWidgets.kTextView).getEntry();
    // taBoard = valuesLayout.add("Target Area", 0).withWidget(BuiltInWidgets.kTextView).getEntry();
    // tvBoard = limelightLayout.add("Has Target", false).withWidget(BuiltInWidgets.kBooleanBox).getEntry();
    // llMode = limelightLayout.add("Limelight Mode", "NULL").withWidget(BuiltInWidgets.kTextView).getEntry();
    // visionUtilCommand = limelightLayout.add("VisionUtil Command", "None").withWidget(BuiltInWidgets.kTextView).getEntry();
  }

  public void updateShuffleboard() {
    // txBoard.setDouble(getTargetX());
    // tyBoard.setDouble(getTargetY());
    // taBoard.setDouble(getTargetArea());
    // tvBoard.setBoolean(hasTarget());
    // llMode.setString(getLimelightMode().name());
    // visionUtilCommand.setString(getCurrentCommand() != null ? getCurrentCommand().getName() : "None");
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    updateShuffleboard();
  }
}
