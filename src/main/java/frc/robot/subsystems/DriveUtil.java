// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
// import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.SPI.Port;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardLayout;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.RobotContainer;

public class DriveUtil extends SubsystemBase {

  private WPI_TalonSRX leftPrimary, leftSecondary, rightPrimary, rightSecondary;
  private DifferentialDrive differentialDrive;
  private double speed, rotation, left, right;
  private double targetSpeed, previousSpeed = 0;
  private ADXRS450_Gyro gyro;
  // private AHRS navX;
  private Encoder leftEncoder, rightEncoder;

  int cpuCounter = 0;

  // C2R values
  double inputCenter, outputCenter;
  double scale, result;
  
  // autoStraight
  double adjustment, leftSpeed, rightSpeed;

  // SHUFFLEBOARD
  ShuffleboardTab sensorTab;
  ShuffleboardLayout driveUtilData, navXValues, encoderValues;
  NetworkTableEntry navXGyro, navXYaw, navXRoll, leftEncoderValue, rightEncoderValue, currentDriveCommand;

  /** Creates a new DriveUtil. */
  public DriveUtil() {
    leftPrimary = new WPI_TalonSRX(Constants.leftPrimary);
    leftSecondary = new WPI_TalonSRX(Constants.leftSecondary);
    rightPrimary = new WPI_TalonSRX(Constants.rightPrimary);
    rightSecondary = new WPI_TalonSRX(Constants.rightSecondary);

    // Initialize encoders (Quad Encoders using DIO)
    leftEncoder = new Encoder(Constants.leftEncoder_channelA, Constants.leftEncoder_channelB);
    rightEncoder = new Encoder(Constants.rightEncoder_channelA, Constants.rightEncoder_channelB);

    leftSecondary.follow(leftPrimary);
    rightSecondary.follow(rightPrimary);

    leftPrimary.setNeutralMode(NeutralMode.Coast);
    rightPrimary.setNeutralMode(NeutralMode.Coast);
    leftSecondary.setNeutralMode(NeutralMode.Coast);
    rightSecondary.setNeutralMode(NeutralMode.Coast);

    // Invert motors
    // leftPrimary.setInverted(true);
    // leftSecondary.setInverted(true);
    rightPrimary.setInverted(true);
    rightSecondary.setInverted(true);

    differentialDrive = new DifferentialDrive(leftPrimary, rightPrimary);

    gyro = new ADXRS450_Gyro(Port.kOnboardCS0);
    gyro.calibrate();
    gyro.reset();
    // navX = new AHRS(SPI.Port.kMXP);
    // navX.calibrate();
    // navX.reset();
  }

  // Drive robot based on controller values

  public void driveRobot() {
    switch (RobotContainer.getDriveMode()) {
      case XBOX:
        speed = RobotContainer.getDriverRightTrigger() - RobotContainer.getDriverLeftTrigger();
        rotation = RobotContainer.getDriverLeftJoystickX();
        speed = Math.pow(speed, 3);
        arcadeDrive(speed, rotation);
      break;
      case J_MARCADE:
        speed = -RobotContainer.getRealRightJoystickY(); // negate due to motors
        rotation = RobotContainer.getRealLeftJoystickZ() * -.7;
        if (rotation < 0) {
          rotation = -Math.pow(rotation, 2);
        } else {
          rotation = Math.pow(rotation, 2);
        }
        if(speed < 0) {
          speed = -Math.pow(speed, 2);
        } else {
          speed = Math.pow(speed, 2);
        }

        if(RobotContainer.getTiltProtect()) {
          targetSpeed = speed;
          if (targetSpeed > previousSpeed) {
            speed = Math.min(previousSpeed + 0.06, targetSpeed);
          } else {
            speed = Math.min(previousSpeed - 0.06, targetSpeed);
          }
        }
        previousSpeed = speed;

        arcadeDrive(speed, rotation);
      break;
      case J_ARCADE:
        speed = -RobotContainer.getRealLeftJoystickY(); // negate due to motors
        rotation = RobotContainer.getRealLeftJoystickX() * -1;
        if(speed < 0) {
          speed = -Math.pow(speed, 2);
        } else {
          speed = Math.pow(speed, 2);
        }
        arcadeDrive(speed, rotation);
        break;
      case J_TANK:
        left = -RobotContainer.getRealLeftJoystickY();
        right = -RobotContainer.getRealRightJoystickY();
        if(left < 0) {
          left = -Math.pow(left, 2);
        } else {
          left = Math.pow(left, 2);
        }
        if(right < 0) {
          right = -Math.pow(right, 2);
        } else {
          right = Math.pow(right, 2);
        }
        tankDrive(left, right);
      break;
    }
  }


  // // Returns angle from NavX MXP
  // public double getNavXAngle() {
  //   return navX.getAngle();
  // }

  // Returns value in ticks of left encoder
  public int getLeftEncoder() {
    return leftEncoder.get() * -1;
  }

  // Returns value in ticks of right encoder
  public int getRightEncoder() {
    return rightEncoder.get();
  }

  // Resets encoder values to zero
  public void resetEncoders() {
    leftEncoder.reset();
    rightEncoder.reset();
  }

  // Resets gyro value to zero
  public void resetGyro() {
    // navX.reset();
    gyro.reset();
  }

  // Returns angle from NavX MXP
  public double getRawAngle() {
    // return navX.getAngle();
    return gyro.getAngle();
  }

  // Sets drive motor idle mode
  public void setDriveNeutralMode(NeutralMode mode){
    leftPrimary.setNeutralMode(mode);
    rightPrimary.setNeutralMode(mode);
    leftSecondary.setNeutralMode(mode);
    rightSecondary.setNeutralMode(mode);
  }

  // Drives robot based on two given speeds for each side
  // Auto Only
  public void tankDrive(double left, double right) {
    differentialDrive.tankDrive(left, right, false);
  }

  public void arcadeDrive(double speed, double rotation) {
    differentialDrive.arcadeDrive(speed, rotation, false);
  }

  // Calculates adjustment value based on input and bounds
  public double coerce2Range(double input, double inputMin, double inputMax, double outputMin, double outputMax) {
    /* Determine the center of the input range and output range */
    inputCenter = Math.abs(inputMax - inputMin) / 2 + inputMin;
    outputCenter = Math.abs(outputMax - outputMin) / 2 + outputMin;

    /* Scale the input range to the output range */
    scale = (outputMax - outputMin) / (inputMax - inputMin);

    /* Apply the transformation */
    result = (input - inputCenter) * scale + outputCenter;

    /* Constrain to the output range */
    return Math.max(Math.min(result, outputMax), outputMin);
  }

  // Drives robot straight, correcting itself based on gyro values
  public void autoStraight(double speed){
    // Determine adjustment based on gyro angle, while feeding in the bounds for both motor controller speeds and gyro angle.
    // adjustment = coerce2Range(getNavXAngle(), Constants.autoC2RGyroInputMin, Constants.autoC2RGyroInputMax, Constants.autoC2RMotorOutputMin, Constants.autoC2RMotorOutputMax);
    // Apply adjustment value to both speed values
    leftSpeed = speed - adjustment;
    rightSpeed = speed + adjustment;

    tankDrive(leftSpeed, rightSpeed);
  }

  // SHUFFLEBOARD
  // public void initShuffleboard(ShuffleboardTab driverTab, ShuffleboardTab sensorTab){
  //   // LAYOUTS
  //   driveUtilData = sensorTab.getLayout("DRIVEUTIL DATA", BuiltInLayouts.kList).withSize(3, 5).withPosition(9, 0);
  //   // navXValues = driveUtilData.getLayout("NavX Values", BuiltInLayouts.kGrid).withProperties(Map.of("Number of columns", 3, "Number of rows", 1));
  //   // encoderValues = driveUtilData.getLayout("Encoder Values", BuiltInLayouts.kGrid).withProperties(Map.of("Number of columns", 2, "Number of rows", 1));

  //   // VALUES
  //   // navXGyro = navXValues.add("Gyro", 0).withWidget(BuiltInWidgets.kTextView).getEntry();
  //   // navXYaw = navXValues.add("Yaw", 0).withWidget(BuiltInWidgets.kTextView).getEntry();
  //   // navXRoll = navXValues.add("Roll", 0).withWidget(BuiltInWidgets.kTextView).getEntry();
  //   leftEncoderValue = encoderValues.add("Left", 0).withWidget(BuiltInWidgets.kTextView).getEntry();
  //   rightEncoderValue = encoderValues.add("Right", 0).withWidget(BuiltInWidgets.kTextView).getEntry();
  //   currentDriveCommand = driveUtilData.add("Current Drive Command", "NULL").withWidget(BuiltInWidgets.kTextView).getEntry();
  // }

  // public void updateShuffleboard(){
  //   // navXGyro.setDouble(getNavXAngle());
  //   // navXYaw.setDouble(navX.getYaw());
  //   // navXRoll.setDouble(navX.getRoll());
  //   leftEncoderValue.setDouble(getLeftEncoder());
  //   rightEncoderValue.setDouble(getRightEncoder());
  //   currentDriveCommand.setString(getCurrentCommand() == null ? "None" : getCurrentCommand().getName());
  //   SmartDashboard.putNumber("Match Time", DriverStation.getMatchTime());
  //   //SmartDashboard.putNumber("CPU Counter", cpuCounter++);
  // }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    // updateShuffleboard();
  }
}
