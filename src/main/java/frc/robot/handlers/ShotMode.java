// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.handlers;

import frc.robot.Constants;

/** Add your docs here. */
public enum ShotMode {
    OFF(0), CLOSE_LOW(Constants.closeLowRPM), CLOSE_HIGH(Constants.closeHighRPM), 
    TARMAC_HIGH(Constants.tarmacHighRPM), SAFETY_HIGH(Constants.safetyHighRPM),
    AUTO_HIGH(Constants.tarmacHighRPM),
    DYNAMIC(0);

    int targetRPM;

    ShotMode(int targetRPM) {
        this.targetRPM = targetRPM;
    }

    public int getTargetRPM() {
        return targetRPM;
    }
}
