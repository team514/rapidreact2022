// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.handlers;

import frc.robot.Constants;

/** Add your docs here. */
public enum LimelightMode {
    VISION_PROCESSOR(Constants.visionCam, Constants.visionLED), DRIVER_CAMERA(Constants.driverCam, Constants.driverLED);

    int camMode, ledMode;

    LimelightMode(int camMode, int ledMode){
        this.camMode = camMode;
        this.ledMode = ledMode;
    }

    public int getCamMode() {
        return camMode;
    }

    public int getLedMode() {
        return ledMode;
    }
}
