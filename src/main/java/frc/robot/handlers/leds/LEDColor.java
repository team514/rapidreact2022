// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.handlers.leds;

/** Add your docs here. */
public enum LEDColor {
    RED(255,0,0), 
    YELLOW(255,255,0), 
    GREEN(0,255,0), 
    BLUE(0,0,255), 
    WHITE(255,255,255), 
    OFF(0,0,0), 
    RAINBOW(0,0,0), 
    PURPLE(255,0,200),
    ORANGE(255,100,0);

    int r, g, b;

    LEDColor(int r, int g, int b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public int getRed() {
        return r;
    }

    public int getGreen() {
        return g;
    }

    public int getBlue() {
        return b;
    }
}
