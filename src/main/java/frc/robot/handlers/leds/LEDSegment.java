// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.handlers.leds;

import frc.robot.Constants;

/** Add your docs here. */
public enum LEDSegment {
    LEFT_TOP(Constants.sideSegmentLength * Constants.leftTopPos),
    LEFT_BOTTOM(Constants.sideSegmentLength * Constants.leftBottomPos),
    RIGHT_TOP(Constants.sideSegmentLength * Constants.rightTopPos),
    RIGHT_BOTTOM(Constants.sideSegmentLength * Constants.rightBottomPos),
    UNDER(Constants.sideSegmentLength * Constants.underPos);

    int bufferOffset;

    LEDSegment(int bufferOffset) {
        this.bufferOffset = bufferOffset;
    }

    public int getBufferOffset() {
        return bufferOffset;
    }
}
